import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = environment.apiUrl;
const API_KEY = environment.apiKey;
const API_URLbirds = environment.apiUrlBirds;

@Injectable({
  providedIn: 'root'
})
export class BirdAPIService {

  birds: any;
  regionCodeJsonPath = '../assets/data/regionCode.json';

  constructor(private http: HttpClient) { }

  getData(url): Observable<any> {
    var address = `${API_URL}/${url}/${API_KEY}`;
    console.log(address);
    return this.http.get(address);
  }

  retrieveCountries() {
    return new Promise((resolve, reject) => {
      this.http.get(this.regionCodeJsonPath).subscribe((data) => {
        resolve(data)}),
        error => {
          reject("error");
        }
    });
  }


  getRecentObservationsInARegion(regionCode: string){
    return new Promise((resolve, reject) => {
      this.http.get(API_URL + regionCode + "/" +  "recent" + API_KEY).subscribe(data => {
        resolve(data)}),
        error => {
          reject("Error retrieving data");
        }
      });
    // apiUrl + regionCode + "/recent" + apiKey 
    // https://ebird.org/ws2.0/data/obs/{{regionCode}}/recent?key=qr6n8qfqboep
  }

  getAllBirds() {
    return new Promise((resolve, reject) => {
      this.http.get(API_URLbirds).subscribe(data => {
        resolve(data)
      }), error => {
        reject("Error retrieving data");
      }
    });
  }

  
}
