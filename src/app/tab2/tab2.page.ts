import { Component, OnInit } from '@angular/core';
import { BirdAPIService } from '../bird-api.service';
import { NavigationServiceService } from '../navigation-service.service';
import { NavController, NavParams } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

import { Map, latLng, tileLayer, Layer, marker } from 'leaflet';
import L from 'leaflet';
import 'leaflet/dist/images/marker-shadow.png';
import 'leaflet/dist/images/marker-icon-2x.png';

import { Geolocation } from '@ionic-native/geolocation/ngx'; 

import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  lat: number;
  lng: number;

  marker: any;

  comName: string;
  howManySeen: number;
  latitudeCo: number;
  longitudeCo: number;
  locationName: string;
  timeAndDateObserved: string;
  speciesScientificName: string;
  speciesCode: string;

  markers: any = [];

  regionCodeList: any;
  recentObservations: any;

  selectedRegionCode: string;

  constructor(private http: HttpClient, public birdApiService: BirdAPIService, public navCtrl: NavController,
    public navigationService: NavigationServiceService,
              private geolocation: Geolocation) {
  }

  ionViewWillEnter () {

    this.retrieveCountries();

    this.comName = this.navigationService.comName;
    this.howManySeen = this.navigationService.howManySeen;
    this.latitudeCo = this.navigationService.latitudeCo;
    this.longitudeCo = this.navigationService.longitudeCo;
    this.locationName = this.navigationService.locationName;
    this.timeAndDateObserved = this.navigationService.timeAndDateObserved;
    this.speciesScientificName = this.navigationService.speciesScientificName;
    this.speciesCode = this.navigationService.speciesCode;

    console.log(this.comName);
    console.log(this.latitudeCo);
    console.log(this.longitudeCo);

  }

  placeMarkersOnBirds(regionCode: string) {
    this.selectedRegionCode = regionCode;
    this.birdApiService.getRecentObservationsInARegion(this.selectedRegionCode).then(data => {
      this.recentObservations = data;
      console.log(this.recentObservations);
      // console.log(this.recentObservations[4]);
      this.printCoordinates();
      this.drawMap();
      // this.drawMarkers();
      // this.updateMap();
    }, err => {
      alert("error");
    });
  }

  retrieveCountries() {
    this.birdApiService.retrieveCountries().then(data => {
      this.regionCodeList = data;
      this.sortCountries('location', true);
      console.log(this.regionCodeList.regionCodes);
    });
  }

  sortCountries(prop, asc) {
    this.regionCodeList.regionCodes.sort(function(a, b) {
      if (asc) {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
      } else {
        return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
      }
    });
  }

  ngOnInit() {
    // this.geolocation.getCurrentPosition().then((resp) => {
    //   console.log("onInit " + resp.coords.latitude);
    //   console.log(resp.coords.longitude);
    //   this.lat = resp.coords.latitude;
  //   //   this.lng = resp.coords.longitude;
  //     // this.loadmap();
   
  //  }).catch((error) => {
  //    console.log('Error getting location', error);
  //  })
  this.drawMap();
  }

  drawMap() {
     mapboxgl.accessToken = 'pk.eyJ1IjoiZGltYWNpbmF0b3IiLCJhIjoiY2p1djFsNmxpMDY3NDQ0bGhsZWVxMG11NSJ9.eGvEsG-MaWMEL337WWlj-g';
     var map = new mapboxgl.Map({
     container: 'map',
     style: 'mapbox://styles/mapbox/streets-v11'
     });

     this.drawMarkers(map);
  }

  drawMarkers(map) {
    for(let i = 0; i < this.recentObservations.length; i++) {
      var popup = new mapboxgl.Popup({ offset: 25})
      .setHTML(
// tslint:disable-next-line: whitespace
// tslint:disable-next-line: max-line-length
        '<p>This bird is a ' + this.recentObservations[i].comName + '<br> The location is called ' + this.recentObservations[i].locName + '</p>',
      );

      var el = document.createElement('div');
      el.id = 'marker';

      var marker = new mapboxgl.Marker(el)
      .setLngLat([
        this.recentObservations[i].lng,
        this.recentObservations[i].lat
      ])
      .setPopup(popup)
      .addTo(map);
    }
  }

  // drawMarkers() {
  //   for (let i = 0; i < this.recentObservations.length; i++) {

  //     let popup = new mapboxgl.Popup({ offset: 25 });

  //     let el = document.createElement('div');
  //     el.id = 'marker';
  //     let marker = new mapboxgl.Marker(el)
  //     .setLngLat([
  //       this.recentObservations[i].lng,
  //       this.recentObservations[i].lat
  //     ])
  //     .setPopup(popup)
  //     .addTo(this.map);
  //   }
  // }

  printCoordinates() {
    for (let i = 0; i < this.recentObservations.length; i++) {
      console.log("next bird");
      console.log(this.recentObservations[i].lat);
      console.log(this.recentObservations[i].lng);
    }
  }

  // createMarkers(map) {
  //   for (let i = 0; i < this.trains.length; i++) {
  //     // create the popup
  //     var popup = new mapboxgl.Popup({ offset: 25 })
  //     //.setText(this.trains[i].trainNumber);
  //     .setHTML(
  //       '<h5> Train number: ' + this.trains[i].trainNumber + '</h5>'
  //     );
      
  //     // create DOM element for the marker
  //     var el = document.createElement('div');
  //     el.id = 'marker';

  //     var marker = new mapboxgl.Marker(el)
  //     .setLngLat([
  //       this.trains[i].location.coordinates[0],
  //       this.trains[i].location.coordinates[1]
  //     ])
  //     .setPopup(popup) // sets a popup on this marker
  //     .addTo(map);
  //     this.markers.push(marker);
  //   }
  // }

  // loadmap() {
  //   var latVal = this.lat;
  //   var lngVal = this.lng;
  //   setTimeout(() => {
  //     console.log("Loading map");
  //     this.map = new Map('map').setView([latVal, lngVal], 8); 
  //     tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  //       attribution: '',
  //       maxZoom: 18
  //     }).addTo(this.map);

  //     this.map.locate({
  //       setView: true,
  //       maxZoom: 10
  //     }).on('locationfound', (e) => {
  //       console.log('found you');
  //       let markerGroup = L.featureGroup();
  //       let marker: any = L.marker([e.latitude, e.longitude]).on('click', () => {
  //         alert('Marker clicked');
  //       })
  //       markerGroup.addLayer(marker);
  //       this.map.addLayer(markerGroup)
  //       //var marker = new L.marker(latVal,lngVal).addTo(this.map);
  //     //this.map.addLayer(marker);
  //       })
  //     }, 50);
  //   }

    // updateMap() {
    //   for (var i = 0; i < this.recentObservations.length; i++) {
    //      this.marker = new L.marker([
    //       this.recentObservations[i][2],
    //       this.recentObservations[i][3]])
    //       .bindPopup(this.recentObservations[i][0])
    //       .addTo(this.map);
    //   }
    // }
  }
