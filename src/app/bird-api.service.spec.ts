import { TestBed } from '@angular/core/testing';

import { BirdAPIService } from './bird-api.service';

describe('BirdAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BirdAPIService = TestBed.get(BirdAPIService);
    expect(service).toBeTruthy();
  });
});
