import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { BirdAPIService } from '../bird-api.service';
import { NavigationServiceService } from '../navigation-service.service';
import { template } from '@angular/core/src/render3';

import { Tab3Page } from '../tab3/tab3.page';
import {ToastController} from '@ionic/angular';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {

  regionCodeJsonPath = '../assets/data/regionCode.json';
  regionCodeList: any;
  regionCodeListHardCopy: any;
  locationList: any;

  recentObservations: any;

  countryOverlayHidden = true;
  searchbarEmptyResultsHidden = true;
  selectedCountry: string;
  selectedRegionCode: string;

  birdList: any;

  constructor(private http: HttpClient, public birdApiService: BirdAPIService,
    public navCtrl: NavController, public navigationService: NavigationServiceService,
    public toastController: ToastController, public loadingController: LoadingController) {
    this.retrieveCountries();
    this.getBirds();
  }

  ionViewWillEnter() {
    this.navigationService.emptyBirdData();
  }

  retrieveCountries() {
    this.birdApiService.retrieveCountries().then(data => {
      this.regionCodeList = data;
      console.log(this.regionCodeList.regionCodes);
    });
  }

  getBirds() {
    this.birdApiService.getAllBirds().then(data => {
      this.birdList = data;
      this.sortBirds('comName', true);
      this.navigationService.birdList = this.birdList;
      console.log(this.birdList);
    }, err => {
      alert('Error retrieving Data');
    });
  }

  sortBirds(prop, asc) {
    this.birdList.sort(function(a, b) {
      if (asc) {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
      } else {
        return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
      }
    });
  }

  // bird.comName, bird.howMany, bird.lat, bird.lng, bird.locName, bird.obstDt, bird.sciName, bird.speciesCode
  loadBirdInfo(commonName: string, howManySeen: number, latitudeCo: number, longitudeCo: number,
     locationName: string, timeAndDateObserved: string, speciesScientificName: string, speciesCode: string) {
    this.navigationService.comName = commonName;
    this.navigationService.howManySeen = howManySeen;
    this.navigationService.latitudeCo = latitudeCo;
    this.navigationService.longitudeCo = longitudeCo;
    this.navigationService.locationName = locationName;
    this.navigationService.timeAndDateObserved = timeAndDateObserved;
    this.navigationService.speciesScientificName = speciesScientificName;
    this.navigationService.speciesCode = speciesCode;
  }

  public hideOverlay() {
    this.countryOverlayHidden = true;
  }

  ionViewDidLoad() {
  }

  getRecentObservationsInARegion(country: string, regionCode: string) {
    this.recentObservations = null;
    this.selectedCountry = country;
    this.selectedRegionCode = regionCode;
    console.log(this.selectedCountry);
    console.log(this.selectedRegionCode);

    this.presentLoading();
    // Get recent observations from Finland, for an example
    // Recent observations from Finland stored in recentObservations variable and print it
    this.birdApiService.getRecentObservationsInARegion(this.selectedRegionCode).then(data => {
      this.recentObservations = data;
      this.sortObservedBirds('comName', true);
      this.loadingController.dismiss();
      console.log(this.recentObservations);

      if (this.recentObservations.length === 0) {
        this.presentToast();
      }
    }, err => {
      alert('Error retrieving data!');
    });

    this.countryOverlayHidden = false;
  }

  // Sort observed birds into alphabetical order
  sortObservedBirds(prop, asc) {
    this.recentObservations.sort(function(a, b) {
      if (asc) {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
      } else {
        return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
      }
    });
  }

  getItems(event: any) {

    this.searchbarEmptyResultsHidden = false;
    this.regionCodeListHardCopy = this.regionCodeList.regionCodes;
    const query = event.target.value;

    if (query && query.trim() !== '') {
      this.regionCodeListHardCopy = this.regionCodeListHardCopy.filter((location) => {
        return (location.location.toLowerCase().indexOf(query.toLowerCase()) > -1);
      });
    } else {
      this.searchbarEmptyResultsHidden = true;
    }
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'There are no recordings of birds in this country',
      showCloseButton: true,
      position: 'middle',
      closeButtonText: 'OK'
    });

    toast.onDidDismiss().then(() => {
      this.hideOverlay();
    });
    toast.present();
  }

  async presentLoading() {

      const loading = await this.loadingController.create({
        message: 'Looking for birds...',
       // duration: 2000
      });

      await loading.present();

  }

}
