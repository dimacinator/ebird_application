import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavigationServiceService {

  comName: string;
  howManySeen: number;
  latitudeCo: number;
  longitudeCo: number;
  locationName: string;
  timeAndDateObserved: string;
  speciesScientificName: string;
  speciesCode: string;

  birdList: any;

  constructor() { }

  emptyBirdData() {
    this.comName = null;
    this.howManySeen = null;
    this.latitudeCo = null;
    this.longitudeCo = null;
    this.locationName = null;
    this.timeAndDateObserved = null;
    this.speciesScientificName = null;
    this.speciesCode = null;
  }
}
