import { Component } from '@angular/core';
import { BirdAPIService } from '../bird-api.service';
import { NavigationServiceService } from '../navigation-service.service';
import { NavController, NavParams } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  comName: string;
  howManySeen: number;
  latitudeCo: number;
  longitudeCo: number;
  locationName: string;
  timeAndDateObserved: string;
  speciesScientificName: string;
  speciesCode: string;

  familyComName: string;
  familySciName: string;
  order: string;
  sciName: string;

  birdList: any;
  birdListHardcopy: any;

  searchbarEmptyResultsHidden = true;
  isBirdSelected: boolean;
  birdFromList: boolean = false;

  constructor(private http: HttpClient, public birdApiService: BirdAPIService,
    public navCtrl: NavController, public navigationService: NavigationServiceService) {
      this.isBirdSelected = false;
  }

  ionViewWillEnter () {
    this.birdList = this.navigationService.birdList;
    this.comName = this.navigationService.comName;
    this.howManySeen = this.navigationService.howManySeen;
    this.latitudeCo = this.navigationService.latitudeCo;
    this.longitudeCo = this.navigationService.longitudeCo;
    this.locationName = this.navigationService.locationName;
    this.timeAndDateObserved = this.navigationService.timeAndDateObserved;
    this.speciesScientificName = this.navigationService.speciesScientificName;
    this.speciesCode = this.navigationService.speciesCode;

    this.navigationService.emptyBirdData();

    if (this.comName === null) {
      this.isBirdSelected = false;
    } else if (this.comName !== null) {
      this.isBirdSelected = true;
    }

    // console.log(this.comName);
    // console.log(this.howManySeen);
    // console.log(this.latitudeCo);
    // console.log(this.longitudeCo);
    // console.log(this.locationName);
    // console.log(this.timeAndDateObserved);
    // console.log(this.speciesScientificName);
    // console.log(this.speciesCode);

    // this.getBirds();
  }

  selectBird(commonName: string, familyComName: string, familySciName: string, order: string, sciName: string) {
      this.searchbarEmptyResultsHidden = true;
      this.isBirdSelected = false;
      this.birdFromList = true;

      this.comName = commonName;
      this.familyComName = familyComName;
      this.familySciName = familySciName;
      this.order = order;
      this.sciName = sciName;
  }

  ionViewWillLeave () {
    this.birdFromList = false;
  }

  // getBirds() {
  //   this.birdApiService.getAllBirds().then(data => {
  //     this.birdList = data;
  //     this.sortBirds('comName', true);
  //     console.log(this.birdList);
  //   }, err => {
  //     alert('Error retrieving Data');
  //   });
  // }

  birdSearchbar(event: any) {
    this.searchbarEmptyResultsHidden = false;
    this.birdListHardcopy = this.birdList;
    const query = event.target.value;

    if(query && query.trim() !== '') {
      this.birdListHardcopy = this.birdListHardcopy.filter((bird) => {
        return (bird.comName.toLowerCase().indexOf(query.toLowerCase()) > -1);
      });
    } else {
      this.searchbarEmptyResultsHidden = true;
    }

    console.log(this.birdListHardcopy);
  }

  // sortBirds(prop, asc) {
  //   this.birdList.sort(function(a, b) {
  //     if (asc) {
  //       return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
  //     } else {
  //       return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
  //     }
  //   });
  // }

}
